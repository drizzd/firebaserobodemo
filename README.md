# Firebase Robolectric Demo

This is a demo for testing Firebase Android App with Robolectric.

## Setup

Install firebase and start Firestore emulator.

```
$ npm install -g firebase-tools
$ npm install
$ firebase emulators:start --only firestore
```

## Run

Currently the test fails with a timeout:

```
$ ./gradlew test -i
> Task :app:testDebugUnitTest

l.firebaserobodemo.FirebaseTest > testWrite FAILED
    java.util.concurrent.TimeoutException
        at java.util.concurrent.CompletableFuture.timedGet(CompletableFuture.java:1771)
        at java.util.concurrent.CompletableFuture.get(CompletableFuture.java:1915)
        at l.firebaserobodemo.FirebaseTest.testWrite(FirebaseTest.kt:40)
```
